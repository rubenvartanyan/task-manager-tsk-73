package ru.vartanyan.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    USER("User"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    @NotNull RoleType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
