package ru.vartanyan.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.AuthorizedUser;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.service.ProjectService;
import ru.vartanyan.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService service;

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_USER"})
    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        service.add(user.getUserId(), new Task("New Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        service.removeById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER"})
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @PathVariable("id") String id
    ) {
        final Task task = service.findById(user.getUserId(), id);
        return new ModelAndView("task-edit", "task", task);
    }

    @Secured({"ROLE_USER"})
    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user,
            @ModelAttribute("task") Task task, BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        service.add(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        return projectService.findAll(user.getUserId());
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
