package ru.vartanyan.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.vartanyan.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends JpaRepository<Project, String> {

    Project findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<Project> findAllByUserId(final String userId);

    Project findByUserIdAndName(final String userId, final String name);

    void deleteByUserIdAndName(final String userId, final String name);

}
