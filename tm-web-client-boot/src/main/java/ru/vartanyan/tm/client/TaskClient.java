package ru.vartanyan.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskClient {

    public static ITaskEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskEndpointService";
        @NotNull final String ns = "http://endpoint.tm.vartanyan.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskEndpoint soap =
                Service.create(url, name).getPort(ITaskEndpoint.class);
        return soap;
    }

}
