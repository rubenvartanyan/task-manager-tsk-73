package ru.vartanyan.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.endpoint.IAuthEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthClient {

    public static IAuthEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/AuthEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "AuthEndpointService";
        @NotNull final String ns = "http://endpoint.tm.vartanyan.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IAuthEndpoint soap =
                Service.create(url, name).getPort(IAuthEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) soap;
        bindingProvider.getRequestContext()
                .put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return soap;
    }

}
