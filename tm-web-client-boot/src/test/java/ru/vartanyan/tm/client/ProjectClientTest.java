package ru.vartanyan.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.vartanyan.tm.marker.IntegrationCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Result;

import java.util.ArrayList;
import java.util.List;

public class ProjectClientTest {

    private final Project project = new Project("test");

    private final Project project2 = new Project("test2");

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/projects/";

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add("JSESSIONID" + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Before
    public void before() {
        deleteAll();
        create(project);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findTest() {
        Assert.assertEquals(project.getName(), find(project.getId()).getName());
    }

    @Nullable
    private Project find(String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(getHeader());
        @NotNull String url = TASK_URL + "find/" + projectId;
        @NotNull final ResponseEntity<Project> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Project.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createTest() {
        create(project);
        @Nullable final Project projectNew = find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @NotNull
    private void create(Project project) {
        @NotNull String url = TASK_URL + "create";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(project, getHeader());
        restTemplate.postForEntity(url, httpEntity, Project.class);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void update() {
        final Project updatedProject = find(project.getId());
        updatedProject.setName("updated");
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "save";
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(updatedProject, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Project.class
        );
        Assert.assertEquals("updated", find(project.getId()).getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTest() {
        delete(project.getId());
        Assert.assertNull(find(project.getId()));
    }

    private void delete(String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                TASK_URL + "delete/" + projectId;
        @NotNull final HttpEntity<Project> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Project.class
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(project2);
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    private List<Project> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "findAll";
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Project>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Project>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateAll() {
        create(project2);
        final Project updatedProject = find(project.getId());
        updatedProject.setName("updated1");
        final Project updatedProject2 = find(project2.getId());
        updatedProject2.setName("updated2");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "saveAll";
        @NotNull final HttpEntity<List<Project>> httpEntity =
                new HttpEntity<>(updatedList, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Project.class
        );
        Assert.assertEquals("updated1", find(project.getId()).getName());
        Assert.assertEquals("updated2", find(project2.getId()).getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createAll() {
        delete(project.getId());
        List<Project> list = new ArrayList<>();
        list.add(project);
        list.add(project2);
        @NotNull final String url = TASK_URL + "createAll";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<List> httpEntity =
                new HttpEntity<>(list, getHeader());
        restTemplate.postForObject(url, httpEntity, List.class);
        Assert.assertEquals("test", find(project.getId()).getName());
        Assert.assertEquals("test2", find(project2.getId()).getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAllTest() {
        create(project2);
        Assert.assertEquals(2, findAll().size());
        deleteAll();
        Assert.assertEquals(0, findAll().size());
    }

    @NotNull
    private void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Project[]> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                TASK_URL + "deleteAll", HttpMethod.DELETE, httpEntity, Project[].class
        );
    }

}
