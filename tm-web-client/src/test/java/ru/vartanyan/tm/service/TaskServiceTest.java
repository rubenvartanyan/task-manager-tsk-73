package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.exception.EmptyIdException;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskServiceTest {

    @Nullable
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private static String USER_ID;

    @Nullable
    private Task task;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskService.clear();
        task = taskService.add(USER_ID, new Task("Task"));
    }

    @Test
    @Category(UnitCategory.class)
    public void add() {
        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskService.findAll(USER_ID);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskService.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        @NotNull final Task task = taskService.findById(USER_ID, this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskService.findById(USER_ID, "34");
        Assert.assertNull(task);
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Task task = taskService.findById(USER_ID, null);
        Assert.assertNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskService.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        taskService.removeById(null);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByName() {
        @NotNull final Task task = taskService.findByName(USER_ID, "Task");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskService.findByName(USER_ID, "34");
        Assert.assertNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskService.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    @Category(UnitCategory.class)
    public void removeById() {
        taskService.removeById(USER_ID, task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

}
