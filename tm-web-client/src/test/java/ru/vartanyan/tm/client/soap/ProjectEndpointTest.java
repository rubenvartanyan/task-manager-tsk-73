package ru.vartanyan.tm.client.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.vartanyan.tm.api.endpoint.IAuthEndpoint;
import ru.vartanyan.tm.api.endpoint.IProjectEndpoint;
import ru.vartanyan.tm.marker.WebCategory;
import ru.vartanyan.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ProjectEndpointTest {

    private final Project project = new Project("test");

    private final Project project2 = new Project("test2");

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint endpoint;

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthClient.getInstance("http://localhost:8080");
        Assert.assertTrue(
                authEndpoint.login("user", "user").getSuccess()
        );
        endpoint = ProjectClient.getInstance("http://localhost:8080");
        setSession();
    }

    private static void setSession() throws MalformedURLException {
        @NotNull final BindingProvider bindingProvider =
                (BindingProvider) authEndpoint;
        @NotNull final ObjectMapper oMapper = new ObjectMapper();
        @NotNull final Object headers = bindingProvider.getResponseContext()
                .get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final Map<String, String> mapHeaders =
                oMapper.convertValue(headers, Map.class);
        @NotNull final Object cookieValue =
                mapHeaders.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> collection = (List<String>) cookieValue;
        ((BindingProvider) endpoint).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                Collections.singletonMap(
                        "Cookie",
                        Collections.singletonList(collection.get(0))
                )
        );
    }

    @Before
    public void before() {
        endpoint.create(project);
    }

    @After
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(project.getName(), endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("updated");
        endpoint.save(updatedProject);
        Assert.assertEquals("updated", endpoint.find(project.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(project.getId());
        Assert.assertNull(endpoint.find(project.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(project2);
        final Project updatedProject = endpoint.find(project.getId());
        updatedProject.setName("updated1");
        final Project updatedProject2 = endpoint.find(project2.getId());
        updatedProject2.setName("updated2");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("updated1", endpoint.find(project.getId()).getName());
        Assert.assertEquals("updated2", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(project.getId());
        List<Project> list = new ArrayList<>();
        list.add(project);
        list.add(project2);
        endpoint.createAll(list);
        Assert.assertEquals("test", endpoint.find(project.getId()).getName());
        Assert.assertEquals("test2", endpoint.find(project2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(project2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertNull(endpoint.findAll());
    }

}
