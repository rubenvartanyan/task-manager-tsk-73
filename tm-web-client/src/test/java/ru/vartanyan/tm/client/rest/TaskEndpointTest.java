package ru.vartanyan.tm.client.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.vartanyan.tm.model.Result;
import ru.vartanyan.tm.marker.WebCategory;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskEndpointTest {

    private final Task task = new Task("test");

    private final Task task2 = new Task("test2");

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    final TaskClient endpoint = TaskClient.client();

    @BeforeClass
    public static void beforeClass() {
        auth();
    }

    public static void auth() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add("JSESSIONID" + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Before
    public void before() {
        deleteAll();
        create(task);
    }

    @Test
    @Category(WebCategory.class)
    public void findTest() {
        Assert.assertEquals(task.getName(), find(task.getId()).getName());
    }

    @Nullable
    private Task find(String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(getHeader());
        @NotNull String url = TASK_URL + "find/" + taskId;
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(WebCategory.class)
    public void createTest() {
        create(task);
        @Nullable final Task taskNew = find(task.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task.getId(), taskNew.getId());
    }

    @NotNull
    private void create(Task task) {
        @NotNull String url = TASK_URL + "create";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(task, getHeader());
        restTemplate.postForEntity(url, httpEntity, Task.class);
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Task updatedTask = find(task.getId());
        updatedTask.setName("updated");
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "save";
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(updatedTask, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Task.class
        );
        Assert.assertEquals("updated", find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteTest() {
        delete(task.getId());
        Assert.assertNull(find(task.getId()));
    }

    private void delete(String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                TASK_URL + "delete/" + taskId;
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Task.class
        );
    }

    @Test
    @Category(WebCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(task2);
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    private List<Task> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "findAll";
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Task>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Task>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        create(task2);
        final Task updatedTask = find(task.getId());
        updatedTask.setName("updated1");
        final Task updatedTask2 = find(task2.getId());
        updatedTask2.setName("updated2");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "saveAll";
        @NotNull final HttpEntity<List<Task>> httpEntity =
                new HttpEntity<>(updatedList, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Task.class
        );
        Assert.assertEquals("updated1", find(task.getId()).getName());
        Assert.assertEquals("updated2", find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAllTest() {
        create(task2);
        Assert.assertEquals(2, findAll().size());
        deleteAll();
        Assert.assertEquals(0, findAll().size());
    }

    @NotNull
    private void deleteAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task[]> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                TASK_URL + "deleteAll", HttpMethod.DELETE, httpEntity, Task[].class
        );
    }

}
