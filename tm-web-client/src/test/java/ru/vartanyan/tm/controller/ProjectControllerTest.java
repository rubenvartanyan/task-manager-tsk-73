package ru.vartanyan.tm.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.configuration.WebApplicationConfiguration;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.service.ProjectService;
import ru.vartanyan.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DatabaseConfiguration.class
        }
)
public class ProjectControllerTest {

    private final Project project = new Project("test");

    @NotNull
    @Autowired
    private ProjectService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String USER_ID;

    @Before
    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        service.clear();
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void create() {
        @NotNull String url = "/project/create";
        this.mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Project> list = service.findAll();
        Assert.assertEquals(1, service.findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void delete() {
        service.add(USER_ID, project);
        Assert.assertEquals(1, service.findAll().size());
        @NotNull final String url = "/project/delete/" + project.getId();
        this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
        ).andDo(print())
                .andExpect(status().is3xxRedirection());
        service.findById(project.getId());
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final String url = "/projects";
        this.mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
