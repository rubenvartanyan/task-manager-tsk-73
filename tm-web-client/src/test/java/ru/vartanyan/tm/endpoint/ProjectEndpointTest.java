package ru.vartanyan.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.configuration.WebApplicationConfiguration;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DatabaseConfiguration.class
        }
)
public class ProjectEndpointTest {

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/projects/";

    private final Project project = new Project("test");

    private final Project project2 = new Project("test2");

    @Nullable
    private String USER_ID;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void before() {
        auth();
        deleteAll();
        create(project);
    }

    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
    }

    @Test
    @Category(UnitCategory.class)
    public void findTest() {
        Assert.assertEquals(project.getName(), find(project.getId()).getName());
    }

    @SneakyThrows
    @Nullable
    private Project find(String projectId) {
        @NotNull String url = PROJECT_URL + "find/" + projectId;
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() {
        create(project);
        @Nullable final Project projectNew = find(project.getId());
        Assert.assertNotNull(projectNew);
        Assert.assertEquals(project.getId(), projectNew.getId());
    }

    @SneakyThrows
    @NotNull
    private void create(Project project) {
        @NotNull String url = PROJECT_URL + "create";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(project);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void update() {
        final Project updatedProject = find(project.getId());
        updatedProject.setName("updated");
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(updatedProject);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals("updated", find(project.getId()).getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteTest() {
        delete(project.getId());
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    private void delete(String projectId) {
        @NotNull final String url = PROJECT_URL + "delete/" + projectId;
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(project2);
        Assert.assertEquals(2, findAll().size());
    }

    @SneakyThrows
    @NotNull
    private List<Project> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = this.mockMvc.perform(
                MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        return Arrays.asList(objectMapper.readValue(json, Project[].class));
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void updateAll() {
        create(project2);
        final Project updatedProject = find(project.getId());
        updatedProject.setName("updated1");
        final Project updatedProject2 = find(project2.getId());
        updatedProject2.setName("updated2");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        @NotNull final String url = PROJECT_URL + "saveAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(updatedList);
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals("updated1", find(project.getId()).getName());
        Assert.assertEquals("updated2", find(project2.getId()).getName());
    }

    @SneakyThrows
    @Test
    @Category(UnitCategory.class)
    public void createAll() {
        delete(project.getId());
        List<Project> list = new ArrayList<>();
        list.add(project);
        list.add(project2);
        @NotNull final String url = PROJECT_URL + "createAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(list);
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(url)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
        Assert.assertEquals("test", find(project.getId()).getName());
        Assert.assertEquals("test2", find(project2.getId()).getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteAllTest() {
        create(project2);
        Assert.assertEquals(2, findAll().size());
        deleteAll();
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    @NotNull
    private void deleteAll() {
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(PROJECT_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

}
