package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class UserRepositoryTest {

    @Nullable
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private User user;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        @NotNull final User user = new User();
        user.setLogin("UserTest");
        this.user = userRepository.save(user);
    }

    @After
    public void after() {
        if (userRepository.findById(user.getId()).isPresent())
            userRepository.deleteById(user.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void add() {
        @Nullable final User userById = userRepository.findById(user.getId()).orElse(null);
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        Assert.assertNotNull(userRepository.findByLogin("UserTest"));
    }


    @Test
    @Category(UnitCategory.class)
    public void remove() {
        userRepository.deleteById(user.getId());
        Assert.assertFalse(userRepository.findById(user.getId()).isPresent());
    }

}