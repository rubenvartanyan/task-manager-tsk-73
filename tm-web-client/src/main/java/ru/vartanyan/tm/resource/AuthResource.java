package ru.vartanyan.tm.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.vartanyan.tm.model.Result;
import ru.vartanyan.tm.model.AuthorizedUser;
import ru.vartanyan.tm.service.UserService;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthResource {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (Exception e) {
            return new Result(e);
        }
    }

    @GetMapping("/session")
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/profile")
    public AuthorizedUser user(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        return user;
    }

    @GetMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
