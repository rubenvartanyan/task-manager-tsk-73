package ru.vartanyan.tm.exception;

public class NoSuchTaskException extends AbstractException {

    public NoSuchTaskException() {
        super("Error! No such task found...");
    }

}
