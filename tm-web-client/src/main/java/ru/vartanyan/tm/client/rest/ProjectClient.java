package ru.vartanyan.tm.client.rest;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.model.Project;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

public interface ProjectClient {

    static ProjectClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                // .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, "http://localhost:8080/api/projects");
    }

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") final String id);

    @PostMapping("/create")
    Project create(@RequestBody final Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody final List<Project> projects);

    @PutMapping("/save")
    Project save(@RequestBody final Project project);

    @PutMapping("/saveAll")
    List<Project> saveAll(@RequestBody final List<Project> projects);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    void deleteAll();

}
