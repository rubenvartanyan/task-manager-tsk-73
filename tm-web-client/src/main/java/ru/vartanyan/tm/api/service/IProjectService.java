package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IRecordService<Project> {

    Project findByName(final String userId, final String name);

    Project findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    Project updateById(final String userId, final String id, final String name, final String description);

    Project updateByIndex(final String userId, final Integer index, final String name, final String description);

    Project startById(final String userId, final String id);

    Project startByIndex(final String userId, final Integer index);

    Project startByName(final String userId, final String name);

    Project finishById(final String userId, final String id);

    Project finishByIndex(final String userId, final Integer index);

    Project finishByName(final String userId, final String name);

    Project add(String userId, String name, String description);

    List<Project> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<Project> collection);

    Project add(String userId, @Nullable Project entity);

    Project findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable Project entity);

}
