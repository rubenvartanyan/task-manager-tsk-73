package ru.vartanyan.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vartanyan.tm.model.Role;

import java.util.List;

public interface IRoleRepository extends JpaRepository<Role, String> {

    List<Role> findAllByUserId(final String UserId);

    void deleteByUserId(final String UserId);

}
