package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.repository.dto.IProjectRepository;
import ru.vartanyan.tm.repository.dto.ITaskRepository;
import ru.vartanyan.tm.repository.dto.IUserRepository;
import ru.vartanyan.tm.util.HashUtil;

import java.util.List;

@Service
public final class UserService extends AbstractService<User>
        implements IUserService {

    @NotNull
    @Autowired
    public IUserRepository userRepository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final User user) {
        if (user == null) throw new NullObjectException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.save(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null) throw new EmptyLoginException();
        if (password == null) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        add(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null) throw new EmptyLoginException();
        
        if (password == null) throw new EmptyPasswordException();
        if (email == null) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null) throw new EmptyLoginException();
        
        if (password == null) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public @Nullable User findOneByLogin(
            @Nullable final String login
    ) {
        if (login == null) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        return userRepository.findOneByLogin(login);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null) throw new EmptyLoginException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(true);
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.save(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeByLogin(
            @Nullable final String login
    ) {
        if (login == null) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.removeByLogin(login);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (userId == null) throw new EmptyIdException();
        if (password == null) throw new EmptyPasswordException();
        @NotNull final User user = findOneById(userId);
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.setPasswordHash(hash);
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.save(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null) throw new EmptyLoginException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(false);
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.save(user);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final User user = findOneById(userId);
        if (user == null) throw new NullObjectException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new NullObjectException();
        IUserRepository userRepository = getUserRepository();
        entities.forEach(userRepository::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteAll() {
        @NotNull final IUserRepository userRepository = getUserRepository();
        if (userRepository.findAll().size() != 0) {
                taskService.deleteAll();
                projectService.deleteAll();
                sessionService.deleteAll();
            }
            userRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final User entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.removeOneById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<User> findAll() {
        @NotNull final IUserRepository userRepository = getUserRepository();
        return userRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User findOneById(
            @Nullable final String id
    ) {
        @NotNull final IUserRepository userRepository = getUserRepository();
        if (id == null) throw new EmptyIdException();
        return userRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        userRepository.deleteById(id);
    }

}
