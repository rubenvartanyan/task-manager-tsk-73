package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Properties;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements ru.vartanyan.tm.api.IPropertyService {

    @NotNull
    private final Environment environment;

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_NAME_KEY = "name";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";

    @NotNull
    private static final String JDBC_USER = "jdbc.user";

    @NotNull
    private static final String JDBC_USER_DEFAULT = "root";

    @NotNull
    private static final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT = "Int212121";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_DEFAULT = "jdbc:mysql://localhost:3306/task-manager-tsk";

    @NotNull
    private static final String HBM2DLL_AUTO = "factory.hbm2dllauto";;

    @NotNull
    private static final String HBM2DLL_AUTO_DEFAULT = "update";

    @NotNull
    private static final String DIALECT = "factory.dialect";

    @NotNull
    private static final String DIALECT_DEFAULT = "org.hibernate.dialect.MySQL5InnoDBDialect";

    @NotNull
    private static final String SHOW_SQL = "factory.showsql";

    @NotNull
    private static final String SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String PACKAGES_TO_SCAN_1 = "packagesToScan1";

    @NotNull
    private static final String PACKAGES_TO_SCAN_1_DEFAULT = "ru.vartanyan.tm.dto";

    @NotNull
    private static final String PACKAGES_TO_SCAN_2 = "packagesToScan2";

    @NotNull
    private static final String PACKAGES_TO_SCAN_2_DEFAULT = "ru.vartanyan.tm.model";

    @SneakyThrows
    public PropertyService(@NotNull Environment environment) {
        this.environment = environment;
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return environment.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = environment.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return environment.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public String getJdbcDriver() {
        return environment.getProperty(JDBC_DRIVER, JDBC_DRIVER_DEFAULT);
    }

    @Override
    public String getJdbcUser() {
        return environment.getProperty(JDBC_USER, JDBC_USER_DEFAULT);
    }

    @Override
    public String getJdbcPassword() {
        return environment.getProperty(JDBC_PASSWORD, JDBC_PASSWORD_DEFAULT);
    }

    @Override
    public String getJdbcUrl() {
        return environment.getProperty(JDBC_URL, JDBC_URL_DEFAULT);
    }

    @Override
    public @NotNull String getDialect() {
        return environment.getProperty(DIALECT, DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getHbm2ddlAuto() {
        return environment.getProperty(HBM2DLL_AUTO, HBM2DLL_AUTO_DEFAULT);
    }

    @Override
    public @NotNull String getShowSql() {
        return environment.getProperty(SHOW_SQL, SHOW_SQL_DEFAULT);
    }

    @NotNull
    private static final String USE_LITE_MEMBER_VALUE = "cache.lite-member";

    @NotNull
    private static final String USE_LITE_MEMBER_VALUE_DEFAULT = "true";

    @NotNull
    private static final String USE_MINIMAL_PUTS = "cache.minimal-puts";

    @NotNull
    private static final String USE_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String USE_QUERY_CACHE = "cache.query";

    @NotNull
    private static final String USE_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String USE_SECOND_LEVEL_CACHE = "cache.level";

    @NotNull
    private static final String USE_SECOND_LEVEL_CACHE_DEFAULT = "true";

    @NotNull
    private static final String CACHE_PROVIDER_CONFIG = "cache.config";

    @NotNull
    private static final String CACHE_PROVIDER_CONFIG_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String CACHE_REGION_FACTORY = "cache.factory";

    @NotNull
    private static final String CACHE_REGION_FACTORY_DEFAULT
            = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String CACHE_REGION_PREFIX = "cache.prefix";

    @NotNull
    private static final String CACHE_REGION_PREFIX_DEFAULT = "tm";

    @NotNull
    public static final String USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    @Override
    public String getUseLiteMemberValue() {
        return environment.getProperty(USE_LITE_MEMBER_VALUE, USE_LITE_MEMBER_VALUE_DEFAULT);
    }

    @NotNull
    @Override
    public String getUseMinimalPuts() {
        return environment.getProperty(USE_MINIMAL_PUTS, USE_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getUseQueryCache() {
        return environment.getProperty(USE_QUERY_CACHE, USE_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getUseSecondLevelCache() {
        return environment.getProperty(USE_SECOND_LEVEL_CACHE, USE_SECOND_LEVEL_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return environment.getProperty(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return environment.getProperty(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return environment.getProperty(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return environment.getProperty(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getCacheProviderConfig() {
        return environment.getProperty(CACHE_PROVIDER_CONFIG, CACHE_PROVIDER_CONFIG_DEFAULT);
    }

    @NotNull
    @Override
    public String getCacheRegionFactory() {
        return environment.getProperty(CACHE_REGION_FACTORY, CACHE_REGION_FACTORY_DEFAULT);
    }

    @NotNull
    @Override
    public String getCacheRegionPrefix() {
        return environment.getProperty(CACHE_REGION_PREFIX, CACHE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getPackagesToScan1() {
        return environment.getProperty(PACKAGES_TO_SCAN_1, PACKAGES_TO_SCAN_1_DEFAULT);
    }

    @NotNull
    @Override
    public String getPackagesToScan2() {
        return environment.getProperty(PACKAGES_TO_SCAN_2, PACKAGES_TO_SCAN_2_DEFAULT);
    }


}
