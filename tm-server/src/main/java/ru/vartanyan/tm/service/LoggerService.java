package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;


public class LoggerService implements ILoggerService {

    @NotNull
    public static final String COMMANDS = "COMMANDS";
    @NotNull
    public static final String COMMANDS_FILE = "./listeners.txt";

    @NotNull
    public static final String ERRORS = "ERRORS";
    @NotNull
    public static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    public static final String MESSAGES = "MESSAGES";
    @NotNull
    public static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    public static final String RESOURCE = "/logger.properties";

    @NotNull
    public final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull
    public final Logger errors = Logger.getLogger(ERRORS);
    @NotNull
    public final Logger messages = Logger.getLogger(MESSAGES);
    @NotNull
    public final Logger root = Logger.getLogger("");
    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
        registry(errors, ERRORS_FILE, true);
    }

    @Override
    public void command(@Nullable final String message) {
        if ((message).isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if ((message).isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void info(@Nullable final String message) {
        if ((message).isEmpty()) return;
        messages.info(message);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(@NotNull final LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(RESOURCE));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            @NotNull final Boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
