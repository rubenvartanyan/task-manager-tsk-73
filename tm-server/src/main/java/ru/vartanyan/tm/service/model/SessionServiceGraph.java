package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.model.ISessionServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.*;
import ru.vartanyan.tm.model.SessionGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionServiceGraph extends AbstractServiceGraph<SessionGraph>
        implements ISessionServiceGraph {

    @NotNull
    @Autowired
    public ISessionRepositoryGraph sessionRepositoryGraph;

    @NotNull
    public ISessionRepositoryGraph getSessionRepositoryGraph() {
        return sessionRepositoryGraph;
    }

    @NotNull
    @Autowired
    private IUserServiceGraph userServiceGraph;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        sessionRepositoryGraph.save(session);
    }

    @SneakyThrows
    @Transactional
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || password == null) return false;
        final @NotNull UserGraph user = userServiceGraph.findByLogin(login);
        if (user.getLocked()) throw new UserLockedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        final @NotNull UserGraph user = userServiceGraph.findByLogin(login);
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user);
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        sessionRepositoryGraph.save(signSession);
        return session;
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        if (sessionRepositoryGraph.getOne(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session,
                              @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUser().getId() == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull UserGraph user = userServiceGraph.findOneById(session.getUser().getId());
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public SessionGraph close(@Nullable final SessionGraph session) {
        if (session == null) return null;
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        sessionRepositoryGraph.deleteById(session.getId());
        return session;
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<SessionGraph> entities) {
        if (entities == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        entities.forEach(sessionRepositoryGraph::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteAll() {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        sessionRepositoryGraph.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(final @Nullable SessionGraph entity) {
        if (entity == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        sessionRepositoryGraph.deleteById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public @Nullable List<SessionGraph> findAll() {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        return sessionRepositoryGraph.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public SessionGraph findOneById(
            @Nullable final String id
    ) {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        return sessionRepositoryGraph.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        sessionRepositoryGraph.deleteById(id);
    }

}
