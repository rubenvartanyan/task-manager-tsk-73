package ru.vartanyan.tm.exception.system;

public class WrongPasswordException extends Exception{

    public WrongPasswordException() {
        super("Error! Wrong password...");
    }

}
