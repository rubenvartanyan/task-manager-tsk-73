package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.api.endpoint.IUserEndpoint;
import ru.vartanyan.tm.api.service.dto.ISessionService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @Nullable
    @WebMethod
    public User findUserOneBySession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        return userService.findOneById(session.getUserId());
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        sessionService.validate(session);
        return userService.findOneByLogin(login);
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        userService.setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) throws AccessDeniedException, AccessDeniedException {
        sessionService.validate(session);
        if (session == null) throw new AccessDeniedException();
        userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
