package ru.vartanyan.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.Task;

import javax.persistence.EntityManager;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    Task findOneById(@Nullable final String id);

    void removeOneById(@Nullable final String id);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(String userId);

    @NotNull
    List<Task> findAllByProjectIdAndUserId(@Nullable final String userId,
                                           @Nullable final String projectId);

    void removeAllByProjectIdAndUserId(
            @Nullable final String userId,
            @Nullable final String projectId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :taskId")
    void bindTaskByProjectId(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId,
            @Param("taskId") @NotNull final String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskFromProjectId(@Param("userId") @NotNull final String userId,
                                 @Param("id") @NotNull final String id);

    @Nullable
    Task findOneByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    );

    @Nullable Task findOneByUserIdAndName(
            @Nullable final String userId,
            @Nullable final String name
    );

    void removeOneByNameAndUserId(@Nullable final String userId,
                                  @Nullable final String name);

    void deleteAllByUserId(@Nullable final String userId);

    void removeOneByIdAndUserId(@Nullable final String userId,
                                @Nullable final String id);


}
