package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getPackagesToScan1();

    @NotNull
    String getPackagesToScan2();

    String getJdbcDriver();

    String getJdbcUser();

    String getJdbcPassword();

    String getJdbcUrl();

    @NotNull
    String getDialect();

    @NotNull
    String getHbm2ddlAuto();

    @NotNull
    String getShowSql();

    @NotNull String getUseLiteMemberValue();

    @NotNull String getUseMinimalPuts();

    @NotNull String getUseQueryCache();

    @NotNull String getUseSecondLevelCache();

    @NotNull String getCacheProviderConfig();

    @NotNull String getCacheRegionFactory();

    @NotNull String getCacheRegionPrefix();

}
