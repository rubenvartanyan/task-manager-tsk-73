package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.api.service.model.IProjectTaskServiceGraph;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.service.TestUtil;

public class ProjectTaskServiceGraphTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectServiceGraph projectService =
            context.getBean(IProjectServiceGraph.class);

    @NotNull
    private final IUserServiceGraph userService =
            context.getBean(IUserServiceGraph.class);

    @NotNull
    private final ITaskServiceGraph taskService =
            context.getBean(ITaskServiceGraph.class);

    @NotNull
    private final IProjectTaskServiceGraph projectTaskService =
            context.getBean(IProjectTaskServiceGraph.class);

    @Before
    public void before() {
        userService.deleteAll();
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUser(userGraph);
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(taskService.findOneByUserIdAndId(taskId, userId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUser(userGraph);
        task.setProject(project);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectIdAndUserId(projectId, userId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());

        final TaskGraph task2 = new TaskGraph();
        task2.setUser(userGraph);
        task2.setProject(project);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());

        final TaskGraph task3 = new TaskGraph();
        final @NotNull UserGraph userGraph2 = userService.findByLogin("test2");
        final String user2Id = userGraph2.getId();
        task3.setUser(userGraph2);
        task3.setProject(project);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(projectId, user2Id).size());

        final TaskGraph task4 = new TaskGraph();
        final ProjectGraph project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUser(userGraph);
        task4.setProject(project2);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectIdAndUserId(project2Id, userId).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final TaskGraph task2 = new TaskGraph();
        final TaskGraph task3 = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final ProjectGraph project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUser(userGraph);
        task.setProject(project);
        task2.setUser(userGraph);
        task2.setProject(project);
        task3.setUser(userGraph);
        task3.setProject(project);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectIdAndUserId(projectId, userId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectIdAndUserId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() throws NullObjectException {
        final TaskGraph task = new TaskGraph();
        final @NotNull UserGraph userGraph = userService.findByLogin("test");
        final String userId = userGraph.getId();
        final String taskId = task.getId();
        task.setUser(userGraph);
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(taskService.findOneByUserIdAndId(taskId, userId) != null);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final TaskGraph task2 = taskService.findOneByUserIdAndId(taskId, userId);
        Assert.assertNull(task2.getProject());
    }

}
