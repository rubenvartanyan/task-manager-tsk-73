package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void info(@NotNull final String message);

    void debug(@NotNull final String message);

    void command(@NotNull final String message);

    void error(@NotNull final Exception e);

}
