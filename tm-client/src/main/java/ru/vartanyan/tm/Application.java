package ru.vartanyan.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.util.SystemUtil;

public class Application {

    public static void main(final String[] args) throws Exception {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
