package ru.vartanyan.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractUserListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class UserRemoveByIdListener extends AbstractUserListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove user by Id";
    }

    @Override
    @EventListener(condition = "@userRemoveByIdListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final SessionDTO session = bootstrap.getSession();
        System.out.println("REMOVE USER BY ID");
        System.out.println("[ENTER ID]");
        @NotNull final String userId = TerminalUtil.nextLine();
        adminEndpoint.removeUserById(userId, session);
        System.out.println("[USER REMOVED]");
    }

}
