package ru.vartanyan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractProjectListener;
import ru.vartanyan.tm.endpoint.ProjectDTO;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class ProjectFindByNameListener extends AbstractProjectListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-find-by-name";
    }

    @Override
    public String description() {
        return "Find project by name";
    }

    @Override
    @EventListener(condition = "@projectFindByNameListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[FIND PROJECT BY NAME]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.findProjectByName(name, session);
        System.out.println(project.getName() + ": " + project.getDescription());
    }

}
